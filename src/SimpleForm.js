import React from "react";
import { Field, reduxForm } from "redux-form";
import Section2 from './sections/section2'
import Section3 from './sections/section3'
import Section4 from './sections/section4'
import Section5 from './sections/section5'
import Section6 from './sections/section6'
import Section7 from './sections/section7'
import Section8 from './sections/section8'
import Section9 from './sections/section9'
import Section10 from './sections/section10'
import Section11 from './sections/section11'
import Section12 from './sections/section12'
import Section13 from './sections/section13'
import Section14 from './sections/section14'
import Section15 from './sections/section15'
import Section16 from './sections/section16'
import Section17 from './sections/section17'
import Section18 from './sections/section18'
import { Collapse } from 'antd';

const { Panel } = Collapse;

    class SimpleForm extends React.Component{
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

        render() {
    return (
        <div>


            <Collapse defaultActiveKey={['1']}>
                <Panel header="This is panel header 1" key="1">
                    <Section2/>
                </Panel>
                <Panel header="This is panel header 2" key="2">
                    <Section3/>
                </Panel>
                <Panel header="This is panel header 3" key="3">
                    <Section4/>
                </Panel>
               <Panel header="This is panel header 4" key="4">
                    <Section5/>
                </Panel>

               <Panel header="This is panel header 5" key="5">
                    <Section6/>
                </Panel>

               <Panel header="This is panel header 6" key="6">
                    <Section7/>
                </Panel>

               <Panel header="This is panel header 7" key="7">
                    <Section8/>
                </Panel>
               <Panel header="This is panel header 8" key="8">
                    <Section9/>
                </Panel>
               <Panel header="This is panel header 9" key="9">
                    <Section10/>
                </Panel>
               <Panel header="This is panel header 10" key="10">
                    <Section11/>
                </Panel>
               <Panel header="This is panel header 11" key="11">
                    <Section12/>
                </Panel>
               <Panel header="This is panel header 12" key="12">
                    <Section13/>
                </Panel>
               <Panel header="This is panel header 13" key="13">
                    <Section14/>
                </Panel>
               <Panel header="This is panel header 14" key="14">
                    <Section15/>
                </Panel>
               <Panel header="This is panel header 15" key="15">
                    <Section16/>
                </Panel>
               <Panel header="This is panel header 16" key="16">
                    <Section17/>
                </Panel>
               <Panel header="This is panel header 17" key="17">
                    <Section18/>
                </Panel>

            </Collapse>

        </div>

    );
}

};


export default reduxForm({
    form: "simple", // a unique identifier for this form
})(SimpleForm);
