import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Input, Radio, Select, Checkbox, Button, DatePicker } from "antd";
import axios from "axios";

import A8V from "../helpers/validation";
import {FormHeadSection} from "../helpers/ui";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const { Option } = Select;
const { TextArea } = Input;
const { RangePicker } = DatePicker;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
    }
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 14,
            offset: 6
        }
    }
};

const makeField = Component => ({ input, meta, children, hasFeedback, label, ...rest }) => {
    const hasError = meta.touched && meta.invalid;
    return (
        <FormItem
            {...formItemLayout}
            label={label}
            validateStatus={hasError ? "error" : "success"}
            hasFeedback={hasFeedback && hasError}
            help={hasError && meta.error}
        >
            <Component {...input} {...rest} children={children} />
        </FormItem>
    );
};

const TextBox = makeField(Input);
const ARadioGroup = makeField(RadioGroup);
const ASelect = makeField(Select);
const ACheckbox = makeField(Checkbox);
const ATextarea = makeField(TextArea);
const ARangePicker = makeField(RangePicker);


let garenterDepandants=[];

class SimpleForm extends React.Component{
    state={
        saluationObj:[],
        yesNoObj:[],
        sectionValidator:[],
        stateObject:[],
        districtObj:[],
        ownedObj:[],
        preferredMailingAddressObj:[],
        occupationObj:[],
        employeeTypeObj:[],
        designationObj:[],
        companyTypeObj:[],
        transactionModeObj:[],
        marriedObj:[],
        bloodGroupObj:[],
        casteObj:[],
        religionObj:[],
        educationObj:[],
        constitutionObj:[],
        nationalityObj:[],
        relationObj:[],
        referenceObj:[],
        genderObj:[],
        coApplicantGuarantorObj:[],
        branchDistObj:[]
    }
    loadApiData = async () => {
        // let url = `${constant.hostUrl}/service-app/apis/v1/`;

        let url = "https://09f88c43.ngrok.io/service-app/apis/v1/";
        const apiActions = ["getSaluationObj","getYesNoObj","getStateObject","getDistrictObj","getOwnedObj","getPreferredMailingAddressObj","getOccupationObj","getEmployeeTypeObj","getDesignationObj","getCompanyTypeObj","getTransactionModeObj","getMarriedObj","getBloodGroupObj","getCasteObj","getReligionObj","getEducationObj","getConstitutionObj","getNationalityObj","getRelationObj","getReferenceObj","getGenderObj","getCoApplicantGuarantorObj","getBranchDistObj"];
        const response = await Promise.all(apiActions.map((action) => axios.post(url + action)))

        this.setState({saluationObj: response[0].data, yesNoObj: response[1].data, stateObject: response[2].data, districtObj: response[3].data, ownedObj: response[4].data, preferredMailingAddressObj: response[5].data, occupationObj: response[6].data, employeeTypeObj: response[7].data, designationObj: response[8].data, companyTypeObj: response[9].data, transactionModeObj: response[10].data, marriedObj: response[11].data, bloodGroupObj: response[12].data, casteObj: response[13].data, religionObj: response[14].data, educationObj: response[15].data, constitutionObj: response[16].data, nationalityObj: response[17].data, relationObj: response[18].data, referenceObj: response[19].data, genderObj: response[20].data, coApplicantGuarantorObj: response[21].data,  branchDistObj: response[22].data});
    }
    searchByPincode1=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("residentialState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("residentialCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("residentialDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("individualIncomeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("individualIncomeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("individualIncomeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode2=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("permanentState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("permanentCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("permanentDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj1=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("permanentLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("permanentLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("permanentDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode3=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("officeState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("officeCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("officeDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj2=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("officeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("officeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("officeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeReferenceContact=(name)=>{
        this.props.fieldPopulator("salaryBusinessEmploymentReferencesNumber", {
            type: "String",
            value: name.value
        });
        this.props.fieldPopulator("salaryBusinessBusinessReferencesNumber", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode8=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("contactableAddressDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("contactableAddressDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("contactableAddressDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj3=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("contactableAddressDetailsLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeNativeAddress=(name)=>{
        this.props.fieldPopulator("ocrNativePlaceOfApplicantMentionAddress", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode4=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("nativeDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("nativeDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("nativeDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranches=()=>{
        let branches=[]
        let obj=this.state.branchDistObj;
        obj.forEach(item=>{if(branches.indexOf(item['BANK_BRANCH'])==-1){
            branches.push(item['BANK_BRANCH']);
        }})
        console.error('brnachesakjsdnasj'+JSON.stringify(branches))
        return branches;
    }

    render() {



        return (

            <div>
                <div className="form-section">
                    <FormHeadSection
                        sectionLabel="Other Details"
                        sectionKey="officeDetails"
                        formSyncError={this.props.formSyncError}
                        sectionValidator={this.state.sectionValidator}
                    />
                    <div className="form-section-content" style={{ display: "block" }}>
                        <div className="flex-row">


                            <label className="col-xs-12 col-md-12">
                                <b>Other Details</b>
                            </label>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Marital Status *"
                                    name="otherDetailsMaritalStatus"
                                    component={ASelect}
                                    placeholder="Select Marital Status"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Marital Status is required" })
                                    ]}
                                >
                                    {
                                        this.state.marriedObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>
                            {this.props.formValues && this.props.formValues.otherDetailsMaritalStatus &&
                            this.props.formValues.otherDetailsMaritalStatus.value == 'M' &&

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Spouse Name *"
                                    name="otherDetailsSpouseName"
                                    component={TextBox}
                                    placeholder="Enter Spouse Name"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Spouse Name is required" })
                                    ]}
                                />
                            </div>
                            }
                            {this.props.formValues && this.props.formValues.otherDetailsMaritalStatus &&
                            this.props.formValues.otherDetailsMaritalStatus.value == 'M' &&


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Spouse Age *"
                                    name="otherDetailsSpouseAge"
                                    component={TextBox}
                                    placeholder="Enter Spouse Age"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Spouse Age is required" })
                                    ]}
                                />
                            </div>
                            }
                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Blood Group *"
                                    name="otherDetailsBloodGroup"
                                    component={ASelect}
                                    placeholder="Enter Blood Group"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        // A8V.required({ errorMsg: "Blood Group is required" })
                                    ]}
                                >
                                    {
                                        this.state.bloodGroupObj.map(item=>{
                                            return (<Option value={item}>{item}</Option>)
                                        })}
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Caste *"
                                    name="otherDetailsCaste"
                                    component={ASelect}
                                    placeholder="Select Caste"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "Caste is required" })]}
                                >
                                    {
                                        this.state.casteObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Religion *"
                                    name="otherDetailsReligion"
                                    component={ASelect}
                                    placeholder="Select Religion"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Religion is required" })
                                    ]}
                                >
                                    {
                                        this.state.religionObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>

                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Education *"
                                    name="otherDetailsEducation"
                                    component={ASelect}
                                    placeholder="Select Education"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Education is required" })
                                    ]}
                                >

                                    {
                                        this.state.educationObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Constitution *"
                                    name="otherDetailsConstitution"
                                    component={ASelect}
                                    placeholder="Enter Constitution"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Constitution is required" })
                                    ]}
                                >{
                                    this.state.constitutionObj.map(item=>{
                                        return (<Option value={item} key={item}>{item}</Option>)
                                    })
                                }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Nationality/Residential Status *"
                                    name="otherDetailsNationalityResidentialStatus"
                                    component={ASelect}
                                    placeholder="Enter Nationality/Residential Status"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Nationality/Residential Status is required"
                                        })
                                    ]}
                                >{
                                    this.state.nationalityObj.map(item=>{
                                        return (<Option value={item} key={item}>{item}</Option>)
                                    })
                                }</Field>
                            </div>

                            <label className="col-xs-12 col-md-12">
                                <b>Family Details</b>
                            </label>
                            <div
                                className="form-group col-xs-12 col-md-12"
                                style={{ textAlign: "right" }}
                            >
                                <button
                                    onClick={this.addGarenterDependant}
                                    type="button"
                                    style={{ width: "75px" }}
                                    className="ant-btn button customFocus button-primary button-primary-alt button-xsm ripplesave"
                                >
                                    <span>Add</span>
                                </button>
                            </div>
                            {garenterDepandants.map((item, index) => {
                                return (
                                    <div className="flex-row">
                                        <div className="form-group col-xs-12 col-md-12">
                                            {index > 0 && (
                                                <div
                                                    className="form-group col-xs-12 col-md-1"
                                                    style={{
                                                        textAlign: "right",
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "center"
                                                    }}
                                                >
                                                    <button
                                                        onClick={() =>
                                                            this.removeGarenterDependant(index)
                                                        }
                                                        type="button"
                                                        style={{ width: "100px" }}
                                                        className="ant-btn button customFocus button-primary button-primary-alt button-xsm ripplesave"
                                                    >
                                                        <span>Remove </span>
                                                    </button>
                                                </div>
                                            )}
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Relationship *"
                                                name={"familyDetailsRelationship"+index}
                                                component={ASelect}
                                                placeholder="Enter Relationship"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Relationship is required" })
                                                ]}
                                            >
                                                {
                                                    this.state.relationObj.map(item=>{
                                                        return (<Option value={item['id']} key={item['id']}>{item['value']}</Option>)
                                                    })
                                                }
                                            </Field>
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Name *"
                                                name={"familyDetailsName"+index}
                                                component={TextBox}
                                                placeholder="Enter Name"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Name is required" })
                                                ]}
                                            />
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Age *"
                                                name={"familyDetailsAge"+index}
                                                component={TextBox}
                                                placeholder="Enter Age"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Age is required" })
                                                ]}
                                            />
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Address *"
                                                name={"familyDetailsAddress"+index}
                                                component={TextBox}
                                                placeholder="Enter Address"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Address is required" })
                                                ]}
                                            />
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Occupation *"
                                                name={"familyDetailsOccupation"+index}
                                                component={ASelect}
                                                placeholder="Enter Occupation"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Occupation is required" })
                                                ]}
                                            >
                                                {
                                                    this.state.occupationObj.map(item=>{
                                                        return (<Option value={item} key={item}>{item}</Option>)
                                                    })
                                                }
                                            </Field>
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Income *"
                                                name={"familyDetailsIncome"+index}
                                                component={TextBox}
                                                placeholder="Enter Income"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Income is required" })
                                                ]}
                                            />
                                        </div>
                                        <div className="form-group col-xs-6 col-md-4">
                                            <Field
                                                label="Dependent *"
                                                name={"familyDetailsDependent"+index}
                                                component={TextBox}
                                                placeholder="Enter Dependent"
                                                className="a8Select autocapitalize"
                                                validate={[
                                                    A8V.required({ errorMsg: "Dependent is required" })
                                                ]}
                                            />
                                        </div>

                                    </div>
                                );
                            })}



                            <label className="col-xs-12 col-md-12">
                                <b>Referrence Details</b>
                            </label>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Type *"
                                    name="referenceDetailsType"
                                    component={ASelect}
                                    placeholder="Select Type"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "Type is required" })]}
                                >
                                    {
                                        this.state.referenceObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Name *"
                                    name="referenceDetailsName"
                                    component={TextBox}
                                    placeholder="Enter Name"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "Name is required" })]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Gender *"
                                    name="referenceDetailsGender"
                                    component={ASelect}
                                    placeholder="Select Gender"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Gender is required" })
                                    ]}
                                >
                                    {
                                        this.state.genderObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Contact No. *"
                                    name="referenceDetailsContactNo"
                                    component={TextBox}
                                    onChange={this.changeReferenceContact}
                                    placeholder="Enter Contact No"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Contact No. is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Occupation *"
                                    name="referenceDetailsOccupation"
                                    component={ASelect}
                                    placeholder="Enter Occupation"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Occupation is required" })
                                    ]}
                                >
                                    {
                                        this.state.occupationObj.map(item=>{
                                            return (<Option value={item} key={item}>{item}</Option>)
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Known Since *"
                                    name="referenceDetailsKnownSince"
                                    component={TextBox}
                                    placeholder="Enter Known Since"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Known Since is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Relation *"
                                    name="referenceDetailsRelation"
                                    component={ASelect}
                                    placeholder="Select Relation"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Relation is required" })
                                    ]}
                                >
                                    {
                                        this.state.relationObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Remarks *"
                                    name="referenceDetailsRemarks"
                                    component={TextBox}
                                    placeholder="Enter Remarks"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Remarks is required" })
                                    ]}
                                />
                            </div>
                            <label className="col-xs-12 col-md-12">
                                <b>Contactable Address Details</b>
                            </label>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 1 *"
                                    name="contactableAddressDetailsAddressLine1"
                                    component={TextBox}
                                    placeholder="Enter Address Line 1"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 1 is required" })
                                    ]}
                                />
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 2 *"
                                    name="contactableAddressDetailsAddressLine2"
                                    component={TextBox}
                                    placeholder="Enter Address Line 2"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 2 is required" })
                                    ]}
                                />
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 3 *"
                                    name="contactableAddressDetailsAddressLine3"
                                    component={TextBox}
                                    placeholder="Enter Address Line 3"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 3 is required" })
                                    ]}
                                />
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Landmark *"
                                    name="contactableAddressDetailsLandmark"
                                    component={TextBox}
                                    placeholder="Enter Landmark"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Landmark is required" })
                                    ]}
                                />
                            </div>



                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Pincode *"
                                    name="contactableAddressDetailsPincode"
                                    component={TextBox}
                                    onChange={this.searchByPincode8}
                                    placeholder="Enter Pincode"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Pincode is required" })
                                    ]}
                                />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="District *"
                                    name="contactableAddressDetailsDistrict"
                                    component={ASelect}
                                    placeholder="Select District"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "District is required" })
                                    ]}
                                >
                                    {this.props.formValues && this.props.formValues.contactableAddressDetailsState &&
                                    this.state.districtObj[this.props.formValues.contactableAddressDetailsState.value].map(
                                        item => {
                                            return <Option value={item}>{item}</Option>;
                                        }
                                    )}
                                </Field>
                            </div>



                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="City *"
                                    name="contactableAddressDetailsCity"
                                    component={ASelect}
                                    placeholder="Select City"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "City is required" })]}
                                >
                                    {this.props.formValues && this.props.formValues.contactableAddressDetailsState && this.props.formValues.contactableAddressDetailsState.value &&
                                    this.state.stateObject[this.props.formValues.contactableAddressDetailsState.value] && this.state.stateObject[this.props.formValues.contactableAddressDetailsState.value].map(
                                        item => {
                                            return <Option value={item}>{item}</Option>;
                                        }
                                    )}
                                </Field>
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="State *"
                                    name="contactableAddressDetailsState"
                                    component={ASelect}
                                    placeholder="Select State"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "State is required" })]}
                                >

                                    {Object.keys(this.state.stateObject).map(item => {
                                        return <Option value={item}>{item}</Option>;
                                    })}
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Nearest Branch *"
                                    name="contactableAddressDetailsNearestBranch"
                                    component={ASelect}
                                    placeholder="Select Nearest Branch"
                                    className="a8Select autocapitalize"
                                    onChange={this.getBranchDetailsObj3}
                                    // onChange={this.getBranchDetailsObj("")}
                                    // showSearch
                                    validate={[
                                        A8V.required({ errorMsg: "Nearest Branch is required" })
                                    ]}
                                >
                                    {
                                        this.getBranches().map(item => {
                                            return <Option key={item} value={item}>{item}</Option>;
                                        })
                                    }

                                </Field>
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Latitude *"
                                    name="contactableAddressDetailsLatitude"
                                    component={TextBox}
                                    placeholder="Enter Latitude"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Latitude is required" })
                                    ]}
                                />
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Longitude *"
                                    name="contactableAddressDetailsLongitude"
                                    component={TextBox}
                                    placeholder="Enter Longitude"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Longitude is required" })
                                    ]}
                                />
                            </div>




                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Distance From Branch *"
                                    name="contactableAddressDetailsDistanceFromBranch"
                                    component={TextBox}
                                    placeholder="Enter Distance From Branch"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Distance From Branch is required" })
                                    ]}
                                />
                            </div>


                            <label className="col-xs-12 col-md-12">
                                <b>Native Address Details</b>
                            </label>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address *"
                                    name="nativeDetailsAdFdress"
                                    component={TextBox}
                                    placeholder="Enter Address"
                                    onChange={this.changeNativeAddress}
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Pincode *"
                                    name="nativeDetailsPincode"
                                    onChange={this.searchByPincode4}
                                    component={TextBox}
                                    placeholder="Enter Pincode"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Pincode is required" })
                                    ]}
                                />
                            </div>



                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="State *"
                                    name="nativeDetailsState"
                                    component={ASelect}
                                    placeholder="Select State"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "State is required" })]}
                                >

                                    {Object.keys(this.state.stateObject).map(item => {
                                        return <Option value={item}>{item}</Option>;
                                    })}
                                </Field>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="City *"
                                    name="nativeDetailsCity"
                                    component={ASelect}
                                    placeholder="Select City"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "City is required" })]}
                                >
                                    {this.props.formValues && this.props.formValues.nativeDetailsState && this.props.formValues.nativeDetailsState.value &&
                                    this.state.stateObject[this.props.formValues.nativeDetailsState.value] && this.state.stateObject[this.props.formValues.nativeDetailsState.value].map(
                                        item => {
                                            return <Option value={item}>{item}</Option>;
                                        }
                                    )}
                                </Field>
                            </div>


                            <label className="col-xs-12 col-md-12">
                                <b>Co-applicant/Guarantor</b>
                            </label>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Co-Applicant / Guarantor *"
                                    name="coApplicantCoApplicantGuarantor"
                                    component={TextBox}
                                    placeholder="Enter Co-Applicant / Guarantor"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Co-Applicant / Guarantor is required"
                                        })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Type of Co-Applicant / Guarantor "
                                    name="coApplicantTypeOfCoApplicantGuarantor"
                                    component={ASelect}
                                    placeholder="Enter Type of Co-Applicant / Guarantor"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        // A8V.required({
                                        //   // errorMsg: "Type of Co-Applicant / Guarantor is required"
                                        // })
                                    ]}
                                >
                                    {
                                        this.state.coApplicantGuarantorObj.map(item=>{
                                            return(<Option value={item}>{item}</Option>)
                                        })
                                    }
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Relation with Applicant *"
                                    name="coApplicantRelationWithApplicant"
                                    component={ASelect}
                                    placeholder="Select Relation with Applicant"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Relation with Applicant is required"
                                        })
                                    ]}
                                >
                                    {
                                        this.state.relationObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }

                                </Field>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }


};

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = "Required";
    }

    return errors;
};

export default reduxForm({
    form: "simple", // a unique identifier for this form
    validate
})(SimpleForm);
