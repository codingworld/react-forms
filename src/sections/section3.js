import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Input, Radio, Select, Checkbox, Button, DatePicker } from "antd";
import axios from "axios";

import A8V from "../helpers/validation";
import {FormHeadSection} from "../helpers/ui";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const { Option } = Select;
const { TextArea } = Input;
const { RangePicker } = DatePicker;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
    }
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 14,
            offset: 6
        }
    }
};

const makeField = Component => ({ input, meta, children, hasFeedback, label, ...rest }) => {
    const hasError = meta.touched && meta.invalid;
    return (
        <FormItem
            {...formItemLayout}
            label={label}
            validateStatus={hasError ? "error" : "success"}
            hasFeedback={hasFeedback && hasError}
            help={hasError && meta.error}
        >
            <Component {...input} {...rest} children={children} />
        </FormItem>
    );
};

const TextBox = makeField(Input);
const ARadioGroup = makeField(RadioGroup);
const ASelect = makeField(Select);
const ACheckbox = makeField(Checkbox);
const ATextarea = makeField(TextArea);
const ARangePicker = makeField(RangePicker);


let garenterDepandants=[];

class SimpleForm extends React.Component{
    state={
        saluationObj:[],
        yesNoObj:[],
        sectionValidator:[],
        stateObject:[],
        districtObj:[],
        ownedObj:[],
        preferredMailingAddressObj:[],
        occupationObj:[],
        employeeTypeObj:[],
        designationObj:[],
        companyTypeObj:[],
        transactionModeObj:[],
        marriedObj:[],
        bloodGroupObj:[],
        casteObj:[],
        religionObj:[],
        educationObj:[],
        constitutionObj:[],
        nationalityObj:[],
        relationObj:[],
        referenceObj:[],
        genderObj:[],
        coApplicantGuarantorObj:[],
        branchDistObj:[]
    }
    loadApiData = async () => {
        // let url = `${constant.hostUrl}/service-app/apis/v1/`;

        let url = "https://09f88c43.ngrok.io/service-app/apis/v1/";
        const apiActions = ["getSaluationObj","getYesNoObj","getStateObject","getDistrictObj","getOwnedObj","getPreferredMailingAddressObj","getOccupationObj","getEmployeeTypeObj","getDesignationObj","getCompanyTypeObj","getTransactionModeObj","getMarriedObj","getBloodGroupObj","getCasteObj","getReligionObj","getEducationObj","getConstitutionObj","getNationalityObj","getRelationObj","getReferenceObj","getGenderObj","getCoApplicantGuarantorObj","getBranchDistObj"];
        const response = await Promise.all(apiActions.map((action) => axios.post(url + action)))

        this.setState({saluationObj: response[0].data, yesNoObj: response[1].data, stateObject: response[2].data, districtObj: response[3].data, ownedObj: response[4].data, preferredMailingAddressObj: response[5].data, occupationObj: response[6].data, employeeTypeObj: response[7].data, designationObj: response[8].data, companyTypeObj: response[9].data, transactionModeObj: response[10].data, marriedObj: response[11].data, bloodGroupObj: response[12].data, casteObj: response[13].data, religionObj: response[14].data, educationObj: response[15].data, constitutionObj: response[16].data, nationalityObj: response[17].data, relationObj: response[18].data, referenceObj: response[19].data, genderObj: response[20].data, coApplicantGuarantorObj: response[21].data,  branchDistObj: response[22].data});
    }
    searchByPincode1=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("residentialState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("residentialCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("residentialDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("individualIncomeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("individualIncomeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("individualIncomeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode2=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("permanentState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("permanentCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("permanentDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj1=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("permanentLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("permanentLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("permanentDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode3=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("officeState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("officeCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("officeDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj2=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("officeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("officeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("officeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeReferenceContact=(name)=>{
        this.props.fieldPopulator("salaryBusinessEmploymentReferencesNumber", {
            type: "String",
            value: name.value
        });
        this.props.fieldPopulator("salaryBusinessBusinessReferencesNumber", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode8=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("contactableAddressDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("contactableAddressDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("contactableAddressDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj3=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("contactableAddressDetailsLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeNativeAddress=(name)=>{
        this.props.fieldPopulator("ocrNativePlaceOfApplicantMentionAddress", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode4=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("nativeDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("nativeDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("nativeDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranches=()=>{
        let branches=[]
        let obj=this.state.branchDistObj;
        obj.forEach(item=>{if(branches.indexOf(item['BANK_BRANCH'])==-1){
            branches.push(item['BANK_BRANCH']);
        }})
        console.error('brnachesakjsdnasj'+JSON.stringify(branches))
        return branches;
    }

    render() {



        return (

            <div>
            <div className="form-section">

        <div className="form-section-content" style={{ display: "block" }}>
    <div className="flex-row">

        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="PanCard No *"
        name="panOsvNo"
        component={TextBox}
        placeholder="Enter Pan No"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Pan No is required" })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">

            <Field
        label="Pan Verified*"
        name="panVerified"
        component={ASelect}
        placeholder="Select Pan Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select PAN verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>
            <div className="form-group col-xs-12 col-md-12">
            <div className="flex-row">
            <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Name as per PAN *"
        name="nameAsPanOsv"
        component={TextBox}
        placeholder="Enter Name as per PAN "
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Name as per PAN  is required" })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Pan Status *"
        name="panStatusOsv"
        component={TextBox}
        placeholder="Enter Pan Status"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Pan Status is required" })
                ]}
        />
        </div>
        </div>
        </div>

       <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Aadhar No *"
        name="aadharNo"
        component={TextBox}
        placeholder="Enter Aadhar No"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Aadhar No is required" }),
            A8V.validateAadhar({ errorMsg: "Aadhar No is not valid" })
    ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">

            <Field
        label="Aadhar Verified*"
        name="aadharVerified"
        component={ASelect}
        placeholder="Select Adhar Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select Adhar verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>



        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Voter ID *"
        name="voterNo"
        component={TextBox}
        placeholder="Enter Voter ID"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "VoterID Number is required" }),
            A8V.validVoterId({ errorMsg: "VoterID Number is not valid" })
    ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">

            <Field
        label="VoterID Verified*"
        name="voterVerified"
        component={ASelect}
        placeholder="Select voter Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select VoterID Verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>


        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Ration ID *"
        name="rationId"
        component={TextBox}
        placeholder="Enter Ration ID"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Ration ID is required" })
                ]}
        />
        </div>

        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Ration Verified*"
        name="rationVerified"
        component={ASelect}
        placeholder="Select Ration Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select Ration verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>


        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Passport No *"
        name="customerPassportNo"
        component={TextBox}
        placeholder="Enter Passport No"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({ errorMsg: "Passport No is required" }),
            A8V.validatePassport({ errorMsg: "Aadhar No is not valid" })
    ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Passport Verified*"
        name="passportVerified"
        component={ASelect}
        placeholder="Select Passport Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select Passport verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>


            <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Passport Issue Date*"
        name="passportIssueDate"
        component={DatePicker}
        placeholder="Enter Passport Issue Date"
        validate={[
                A8V.required({
                    errorMsg: "Passport Issue Date is required"
                })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Passport Expiry Date*"
        name="passportExpiryDate"
        component={DatePicker}
        placeholder="Enter Passport Issue Date"
        validate={[
                A8V.required({
                    errorMsg: "Passport Expiry Date is required"
                })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">&nbsp;</div>

        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Driving License No *"
        name="customerDrivingLicenseNo"
        component={TextBox}
        placeholder="Enter Driving License No"
        className="a8Select autocapitalize"
        validate={[
                A8V.required({
                    errorMsg: "Driving License No is required"
                })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Driving License Verified*"
        name="drivingVerified"
        component={ASelect}
        placeholder="Select Ration Verified"
        className="a8Select autocapitalize"
        validate={[
                    A8V.required({ errorMsg: "Select Driving License verified or not" })
                    ]}
            >
            {
                this.state.yesNoObj.map(item => {
                    return <Option value={item['id']}>{item['value']}</Option>;
                })
            }
            </Field>
            </div>
            <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Driving License Issue Date*"
        name="DrivingLicenseIssueDate"
        component={DatePicker}
        placeholder="Enter Driving License Issue Date"
        validate={[
                A8V.required({
                    errorMsg: "Driving License Issue Date is required"
                })
                ]}
        />
        </div>
        <div className="form-group col-xs-6 col-md-4">
            <Field
        label="Driving License Expiry Date*"
        name="DrivingLicenseExpiryDate"
        component={DatePicker}
        placeholder="Enter Driving License Issue Date"
        validate={[
                A8V.required({
                    errorMsg: "Driving License Expiry Date is required"
                })
                ]}
        />
        </div>
        </div>
        </div>
        </div>


            </div>
    );
    }


};

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = "Required";
    }

    return errors;
};

export default reduxForm({
    form: "simple", // a unique identifier for this form
    validate
})(SimpleForm);
