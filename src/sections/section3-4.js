import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Input, Radio, Select, Checkbox, Button, DatePicker } from "antd";
import axios from "axios";

import A8V from "../helpers/validation";
import {FormHeadSection} from "../helpers/ui";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const { Option } = Select;
const { TextArea } = Input;
const { RangePicker } = DatePicker;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 }
    }
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 14,
            offset: 6
        }
    }
};

const makeField = Component => ({ input, meta, children, hasFeedback, label, ...rest }) => {
    const hasError = meta.touched && meta.invalid;
    return (
        <FormItem
            {...formItemLayout}
            label={label}
            validateStatus={hasError ? "error" : "success"}
            hasFeedback={hasFeedback && hasError}
            help={hasError && meta.error}
        >
            <Component {...input} {...rest} children={children} />
        </FormItem>
    );
};

const TextBox = makeField(Input);
const ARadioGroup = makeField(RadioGroup);
const ASelect = makeField(Select);
const ACheckbox = makeField(Checkbox);
const ATextarea = makeField(TextArea);
const ARangePicker = makeField(RangePicker);


let garenterDepandants=[];

class SimpleForm extends React.Component{
    state={
        saluationObj:[],
        yesNoObj:[],
        sectionValidator:[],
        stateObject:[],
        districtObj:[],
        ownedObj:[],
        preferredMailingAddressObj:[],
        occupationObj:[],
        employeeTypeObj:[],
        designationObj:[],
        companyTypeObj:[],
        transactionModeObj:[],
        marriedObj:[],
        bloodGroupObj:[],
        casteObj:[],
        religionObj:[],
        educationObj:[],
        constitutionObj:[],
        nationalityObj:[],
        relationObj:[],
        referenceObj:[],
        genderObj:[],
        coApplicantGuarantorObj:[],
        branchDistObj:[]
    }
    loadApiData = async () => {
        // let url = `${constant.hostUrl}/service-app/apis/v1/`;

        let url = "https://09f88c43.ngrok.io/service-app/apis/v1/";
        const apiActions = ["getSaluationObj","getYesNoObj","getStateObject","getDistrictObj","getOwnedObj","getPreferredMailingAddressObj","getOccupationObj","getEmployeeTypeObj","getDesignationObj","getCompanyTypeObj","getTransactionModeObj","getMarriedObj","getBloodGroupObj","getCasteObj","getReligionObj","getEducationObj","getConstitutionObj","getNationalityObj","getRelationObj","getReferenceObj","getGenderObj","getCoApplicantGuarantorObj","getBranchDistObj"];
        const response = await Promise.all(apiActions.map((action) => axios.post(url + action)))

        this.setState({saluationObj: response[0].data, yesNoObj: response[1].data, stateObject: response[2].data, districtObj: response[3].data, ownedObj: response[4].data, preferredMailingAddressObj: response[5].data, occupationObj: response[6].data, employeeTypeObj: response[7].data, designationObj: response[8].data, companyTypeObj: response[9].data, transactionModeObj: response[10].data, marriedObj: response[11].data, bloodGroupObj: response[12].data, casteObj: response[13].data, religionObj: response[14].data, educationObj: response[15].data, constitutionObj: response[16].data, nationalityObj: response[17].data, relationObj: response[18].data, referenceObj: response[19].data, genderObj: response[20].data, coApplicantGuarantorObj: response[21].data,  branchDistObj: response[22].data});
    }
    searchByPincode1=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("residentialState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("residentialCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("residentialDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("individualIncomeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("individualIncomeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("individualIncomeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode2=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("permanentState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("permanentCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("permanentDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj1=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("permanentLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("permanentLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("permanentDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    searchByPincode3=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("officeState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("officeCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("officeDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj2=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("officeLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("officeLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("officeDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeReferenceContact=(name)=>{
        this.props.fieldPopulator("salaryBusinessEmploymentReferencesNumber", {
            type: "String",
            value: name.value
        });
        this.props.fieldPopulator("salaryBusinessBusinessReferencesNumber", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode8=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("contactableAddressDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("contactableAddressDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("contactableAddressDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranchDetailsObj3=(branchId)=>{
        branchId=branchId.value;
        let obj=this.state.branchDistObj;
        for( var i=0;i<obj.length;i++){
            if(this.state.branchDistObj[i]["BANK_BRANCH"] == branchId){
                let distance=this.calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1);
                this.props.fieldPopulator("contactableAddressDetailsLongitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LONGITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsLatitude", {
                    type: "String",
                    value: this.state.branchDistObj[i]["LATITUDE"]
                });
                this.props.fieldPopulator("contactableAddressDetailsDistanceFromBranch", {
                    type: "String",
                    value: distance
                });
                break;
            }
        }
    }

    changeNativeAddress=(name)=>{
        this.props.fieldPopulator("ocrNativePlaceOfApplicantMentionAddress", {
            type: "String",
            value: name.value
        });
    }

    searchByPincode4=(event)=>{
        if(event.value.length<5)
            return;
        this.getDataByPincode(event.value).then(
            response => {
                console.log({response})
                this.props.fieldPopulator("nativeDetailsState", {
                    type: "String",
                    value: response['data'].state_name
                });
                this.props.fieldPopulator("nativeDetailsCity", {
                    type: "String",
                    value: response['data'].city
                });
                this.props.fieldPopulator("nativeDetailsDistrict", {
                    type: "String",
                    value: response['data'].district
                });
            });
    }

    getBranches=()=>{
        let branches=[]
        let obj=this.state.branchDistObj;
        obj.forEach(item=>{if(branches.indexOf(item['BANK_BRANCH'])==-1){
            branches.push(item['BANK_BRANCH']);
        }})
        console.error('brnachesakjsdnasj'+JSON.stringify(branches))
        return branches;
    }

    render() {



        return (

            <div>
                <div className="form-section">
                    <FormHeadSection
                        sectionLabel="Business / Office Address Details"
                        sectionKey="officeDetails"
                        formSyncError={this.props.formSyncError}
                        sectionValidator={this.state.sectionValidator}
                    />
                    <div className="form-section-content" style={{ display: "block" }}>
                        <div className="flex-row">
                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 1*"
                                    name="officeAddressLine1"
                                    component={TextBox}
                                    placeholder="Enter Address Line 1"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 1 is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 2*"
                                    name="officeAddressLine2"
                                    component={TextBox}
                                    placeholder="Enter Address Line 2"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 2 is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Line 3*"
                                    name="officeAddressLine3"
                                    component={TextBox}
                                    placeholder="Enter Address Line 3"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Address Line 3 is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Landmark*"
                                    name="officeLandmark"
                                    component={TextBox}
                                    placeholder="Enter Landmark"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Landmark is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Village / Taluka*"
                                    name="officeVillageTaluka"
                                    component={TextBox}
                                    placeholder="Enter Village / Taluka"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Village / Taluka is required" })
                                    ]}
                                />
                            </div>



                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Pincode*"
                                    name="officePincode"
                                    onChange={this.searchByPincode3}
                                    component={TextBox}
                                    placeholder="Enter Pincode"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Pincode is required" })
                                    ]}
                                />
                            </div>



                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="State*"
                                    name="officeState"
                                    component={ASelect}
                                    placeholder="Select State"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "State is required" })]}
                                >

                                    {Object.keys(this.state.stateObject).map(item => {
                                        return <Option value={item}>{item}</Option>;
                                    })}
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="District*"
                                    name="officeDistrict"
                                    component={ASelect}
                                    placeholder="Select District"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "District is required" })
                                    ]}
                                >
                                    {this.props.formValues && this.props.formValues.residentialState && this.props.formValues.residentialState.value &&
                                    this.state.districtObj[this.props.formValues.residentialState.value] &&
                                    this.state.districtObj[this.props.formValues.residentialState.value].map(
                                        item => {
                                            return <Option value={item}>{item}</Option>;
                                        }
                                    )}
                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="City*"
                                    name="officeCity"
                                    component={ASelect}
                                    placeholder="Select City"
                                    className="a8Select autocapitalize"
                                    validate={[A8V.required({ errorMsg: "City is required" })]}
                                >
                                    {this.props.formValues && this.props.formValues.officeState && this.props.formValues.officeState.value &&
                                    this.state.stateObject[this.props.formValues.officeState.value] &&
                                    this.state.stateObject[this.props.formValues.officeState.value].map(
                                        item => {
                                            return <Option value={item}>{item}</Option>;
                                        }
                                    )}
                                </Field>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Country*"
                                    name="officeCountry"
                                    component={TextBox}
                                    placeholder="Enter Country"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Country is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Owned/Rented*"
                                    name="officeOwnedRented"
                                    component={ASelect}
                                    placeholder="Select Owned/Rented"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Owned/Rented is required" })
                                    ]}
                                >
                                    {
                                        this.state.ownedObj.map(item => {
                                            return <Option value={item['id']}>{item['value']}</Option>;
                                        })
                                    }

                                </Field>
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Preferred Mailing Address*"
                                    name="officePreferredMailingAddress"
                                    component={TextBox}
                                    placeholder="Enter Preferred Mailing Address"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Preferred Mailing Address is required"
                                        }),
                                        A8V.email({
                                            errorMsg: "Preferred Mailing Address is invalid"
                                        })

                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Landline No*"
                                    name="officeLandlineNo"
                                    component={TextBox}
                                    placeholder="Enter Landline No"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Landline No is required" }),
                                        A8V.validateLandline({ errorMsg: "Landline No is not valid" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Address Validated Through*"
                                    name="officeAddressValidatedThrough"
                                    component={TextBox}
                                    placeholder="Enter Address Validated Through"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Address Validated Through is required"
                                        })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Nearest Branch*"
                                    name="officeNearestBranch"
                                    component={ASelect}
                                    placeholder="Select Nearest Branch"
                                    className="a8Select autocapitalize"
                                    onChange={this.getBranchDetailsObj2}
                                    // onChange={this.getBranchDetailsObj("")}
                                    // showSearch
                                    validate={[
                                        A8V.required({ errorMsg: "Nearest Branch is required" })
                                    ]}
                                >
                                    {
                                        this.getBranches().map(item => {
                                            return <Option key={item} value={item}>{item}</Option>;
                                        })
                                    }

                                </Field>
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Latitude*"
                                    name="officeLatitude"
                                    component={TextBox}
                                    placeholder="Enter Latitude"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Latitude is required" })
                                    ]}
                                />
                            </div>

                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Longitude*"
                                    name="officeLongitude"
                                    component={TextBox}
                                    placeholder="Enter Longitude"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({ errorMsg: "Longitude is required" })
                                    ]}
                                />
                            </div>


                            <div className="form-group col-xs-6 col-md-4">
                                <Field
                                    label="Distance From Branch*"
                                    name="officeDistanceFromBranch"
                                    component={TextBox}
                                    placeholder="Enter Distance From Branch"
                                    className="a8Select autocapitalize"
                                    validate={[
                                        A8V.required({
                                            errorMsg: "Distance From Branch is required"
                                        })
                                    ]}
                                />
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }


};

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = "Required";
    }

    return errors;
};

export default reduxForm({
    form: "simple", // a unique identifier for this form
    validate
})(SimpleForm);
