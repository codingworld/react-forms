// const defaultApiUrl = "https://suryoday-staging.autonom8.com";
const defaultApiUrl = "http://localhost:8080";
// const defaultApiUrl = "https://suryoday.autonom8.com";
export default {
    targetOrigin: "*",
    camundaApi: "",
    hostUrl: defaultApiUrl,
    hostApiUrl: `${defaultApiUrl}/engine-rest/`
}
