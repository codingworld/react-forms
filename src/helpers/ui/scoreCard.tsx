import React from "react";

export const Scorecards = ({ title, Score }) => {
  return (
    <div className="numberWidget">
      <span>{title}</span>
      <span style={{ fontSize: "26px", color: "black" }}>
        {Score}
      </span>
    </div>
  );
};

export const OverallScoreCrad = ({ title, Score }) => {
  return (
    <div  className="overAllScore">
      <span>{title}</span>
      <span style={{ fontSize: "36px", color: "black" }}>
        {Score}
      </span>
    </div>
  );
};

