import Validate from "validate.js";
import { any } from "prop-types";
const required = ({ errorMsg }) => value => {
return false;
    return value && !Validate.isEmpty(value.value) || typeof value === "number"
      ? undefined
      : errorMsg
          ? errorMsg
          : `Required`;

};
const maxLength = ({
                     errorMsg = "",
                     max
                   }: {
  errorMsg?: string; 
  max: number;
}) => value =>{
return false;
    value && value.value.length > max
        ? errorMsg
        ? errorMsg
        : `Must be ${max} characters or less`
        : undefined;
}

const minLength = ({
                     errorMsg = "",
                     min
                   }: {
  errorMsg?: string;
  min: number;
}) => value =>{
    return false;
    value && value.value.length < min
        ? errorMsg
        ? errorMsg
        : `Must be ${min} characters or more`
        : undefined;
// const number = ({ errorMsg }: { errorMsg?: string }) => (value) => {
//   value && isNaN(Number(value.value))
//     ? errorMsg
//       ? errorMsg
//       : "Must be a number"
//     : undefined;
// }
}
const minValue = ({
                    errorMsg = "",
                    min
                  }: {
  errorMsg?: string;
  min: number;
}) => value =>
{
    return false;
    value && value.value < min
        ? errorMsg
        ? errorMsg
        : `Must be at least ${min}`
        : undefined;
}
const maxValue = ({
                    errorMsg = "",
                    max
                  }: {
  errorMsg?: string;
  max: number;
}) => value =>{
    return false;
    value && value.value >= max
        ? errorMsg
        ? errorMsg
        : `Must be lower than ${max}`
        : undefined;
}
const email = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
{return false;
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid email address"
        : undefined;
}
const specialSymbols = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
{return false;    value && !/^(?=[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]*$)(?!.*[<>'"/;@`%])/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
const number = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
    {return false;value && !/^[0-9]*$/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
        const notZero = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
   { return false; value<1
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
        
const validateCibilScore = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
    {return false; !(-1 <= parseInt(value.value) && parseInt(value.value) <= 899)
        ? errorMsg
        ? errorMsg
        : "Invalid cibil score"
        : undefined;}
const validatePanId = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
    {return false;value && !/^(\D{5})(\d{4})(\D{1})$/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
const validVoterId = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
   {return false; value && !/^(\D{3})(\d{7})$/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
const validateAadhar = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
  { return false; value && !/^(\d{12})$/i.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
const validatePassport = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
{return false;value && !/^(\D{1})(\d{7})$/i.test(value.value)
    ? errorMsg
    ? errorMsg
    : "Invalid entry"
    : undefined;}
        const validateMobile = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
    {return false;value && !/^[0][1-9]\d{9}$|^[1-9]\d{9}$/g.test(value.value)
        ? errorMsg
        ? errorMsg
        : "Invalid entry"
        : undefined;}
        const validateLandline = ({ errorMsg = "" }: { errorMsg?: string }) => (value: any) =>
       {return false; value && !/^[0-9]\d{2,4}-\d{6,8}$/i.test(value.value)
            ? errorMsg
            ? errorMsg
            : "Invalid entry"
            : undefined;}
        
export default {
  required,
  maxLength,
  minLength,
  number,
  minValue,
  maxValue,
  email,
  specialSymbols,
  validateCibilScore,
  validatePanId,
  validVoterId,
  validateAadhar,
  validatePassport,
  notZero,validateLandline,
  validateMobile
};
