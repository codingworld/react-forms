import * as React from "react";
import { Checkbox, Form, Divider } from "antd";
import classname from "classnames";

export type checklistProps = {
  input: any;
  fieldKey: string;
  fieldValue: any;
  validation: any;
  meta: any;
};

export interface checklistState { }

class checklist extends React.Component<checklistProps, checklistState> {
  state = {};

  handleChange = (event: any) => {
    if (this.props.validation.includes("required") && !event.target.checked) {
      this.props.input.onChange("");
      this.props.input.onBlur("");
    } else {
      this.props.input.onChange(event.target.checked);
      this.props.input.onBlur(event.target.checked);
    }
  };

  render() {
    let { input, fieldKey, fieldValue, validation, meta } = this.props;
    let { value } = input;
    let { touched, invalid, error } = meta;

    let hasError = touched && invalid;
    return (
      <Form.Item
        validateStatus={hasError ? "error" : "success"}
        style={{ marginBottom: "0px" }}
      >
        <div
          className={classname("list-table-item", { selected: value })}
          style={{ padding: "30px 0 10px 40px" }}
        >
          <label className="input-check input-control">
            <input
              type="checkbox"
              checked={value}
              onChange={e => this.handleChange(e)}
            />

            <span></span>
          </label>
          <div className="row">
            <div className="col-xs-6 col-md-3">
              <p>{fieldKey}</p>
            </div>
            <div className="col-xs-6 col-md-6">
              <p>
                <strong>{fieldValue}</strong>
              </p>
            </div>
          </div>
        </div>

        {hasError && <div className="ant-form-explain">{error}</div>}
      </Form.Item>
    );
  }
}

export default checklist;
