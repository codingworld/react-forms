import React from "react";
import { Field } from "redux-form";
import { Button } from "antd";
import { TextBox, Select, SelectHelper, DatePicker } from "a8flow-uikit";
import {
  A8V,
  proceedNumber
  //retrieveDefaultFiles
  //InjectedFormProps
} from "../../helpers";

const { Option } = SelectHelper;

export const renderPurchaseBill = ({
  fields,
  meta: { touched, error, submitFailed }
}) => (
    <ul className="col-md-12 expenselist">
      <div className="flex-row">
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label="Purchase Bill Month"
            name="PurchaseBillMonth"
            component={DatePicker}
            dateFormat="DD-MM-YYYY"
            placeholder="Enter Purchase Bill Month"
            validate={[
              A8V.required({ errorMsg: "Date is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label="Purchase Bill Type"
            name="PurchaseBillType"
            component={Select}
            placeholder="Select Purchase Bill Type"
            className="a8Select"
            // onChange={this.handleGSTFilingApproach}
            validate={[
              A8V.required({ errorMsg: "Purchase Bill Type is required" }),
            ]}
          >
            <Option value="Pukka">Pukka</Option>
            <Option value="Kuccha">Kuccha</Option>
          </Field>
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label={"Purchase Bill Value"}
            name="PurchaseBillValue"
            component={TextBox}
            placeholder="Enter Purchase Bill Value"
            type="text"
            normalize={proceedNumber}
            hasFeedback
            className="form-control-coustom"
            validate={[
              A8V.required({ errorMsg: "Purchase Bill value is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label="Purchase Bill Date"
            name="PurchaseBillDate"
            component={DatePicker}
            dateFormat="DD-MM-YYYY"
            placeholder="Enter Purchase Bill Date"
            validate={[
              A8V.required({ errorMsg: "Date is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label={"Purchase Margin"}
            name="PurchaseMargin"
            component={TextBox}
            placeholder="Enter Purchase Margin"
            type="text"
            normalize={proceedNumber}
            hasFeedback
            className="form-control-coustom"
            validate={[
              A8V.required({ errorMsg: "Purchase Margin is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label={"Sales by Month"}
            name="SalesByMonth"
            component={TextBox}
            placeholder="Enter Sales By Month"
            type="text"
            normalize={proceedNumber}
            hasFeedback
            className="form-control-coustom"
            validate={[
              A8V.required({ errorMsg: "Sales by Month is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-6 col-md-4">
          <Field
            label={"Average Monthly Sales"}
            name="AverageMonthlySales"
            component={TextBox}
            placeholder="Enter Sales By Month"
            normalize={proceedNumber}
            type="text"
            hasFeedback
            className="form-control-coustom"
            validate={[
              A8V.required({ errorMsg: "Average Monthly Sales is required" }),
            ]}
          />
        </div>
        <div className="form-group col-xs-2 col-md-2">
          <Button
            className="activity-list-item"
            type="danger"
            // style = {{marginTop: "34px"}}
            icon="plus"
            size={"large"}
            onClick={() => fields.push({})}
          />
        </div>
      </div>

      {fields.map((member, index) => (
        <div key={index} className="flex-row">
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label="Purchase Bill Month"
              name={`${member}.PurchaseBillMonth`}
              component={DatePicker}
              dateFormat="DD-MM-YYYY"
              placeholder="Enter Purchase Bill Month"
              validate={[
                A8V.required({ errorMsg: "Date is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label="Purchase Bill Type"
              name={`${member}.PurchaseBillType`}
              component={Select}
              placeholder="Select Purchase Bill Type"
              className="a8Select"
              validate={[
                A8V.required({ errorMsg: "Purchase Bill Type is required" }),
              ]}
            >
              <Option value="Pukka">Pukka</Option>
              <Option value="Kuccha">Kuccha</Option>
            </Field>
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label={"Purchase Bill Value"}
              name={`${member}.PurchaseBillValue`}
              component={TextBox}
              placeholder="Enter Purchase Bill Value"
              normalize={proceedNumber}
              type="text"
              hasFeedback
              className="form-control-coustom"
              validate={[
                A8V.required({ errorMsg: "Purchase Bill value is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label="Purchase Bill Date"
              name={`${member}.PurchaseBillDate`}
              component={DatePicker}
              dateFormat="DD-MM-YYYY"
              placeholder="Enter Purchase Bill Date"
              validate={[
                A8V.required({ errorMsg: "Date is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label={"Purchase Margin"}
              name={`${member}.PurchaseMargin`}
              component={TextBox}
              placeholder="Enter Purchase Margin"
              normalize={proceedNumber}
              type="text"
              hasFeedback
              className="form-control-coustom"
              validate={[
                A8V.required({ errorMsg: "Purchase Margin is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label={"Sales by Month"}
              name={`${member}.SalesByMonth`}
              component={TextBox}
              placeholder="Enter Sales By Month"
              type="text"
              hasFeedback
              className="form-control-coustom"
              validate={[
                A8V.required({ errorMsg: "Sales by Month is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <Field
              label={"Average Monthly Sales"}
              name={`${member}.AverageMonthlySales`}
              component={TextBox}
              normalize={proceedNumber}
              placeholder="Enter Sales By Month"
              type="text"
              hasFeedback
              className="form-control-coustom"
              validate={[
                A8V.required({ errorMsg: "Average Monthly Sales is required" }),
              ]}
            />
          </div>
          <div className="form-group col-xs-6 col-md-4">
            <ul className="horizontal-list">
              <li>
                <Button
                  className="activity-list-item"
                  type="danger"
                  onClick={() => fields.remove(index)}
                  // style={{ marginTop: "33px" }}
                  icon="delete"
                  size={"large"}
                />
              </li>
              <li>
                <Button
                  className="activity-list-item"
                  type="danger"
                  onClick={() => fields.push(index)}
                  // style={{ marginTop: "33px" }}
                  icon="plus"
                  size={"large"}
                />
              </li>
            </ul>
          </div>
        </div>
      ))}
    </ul>
  );
