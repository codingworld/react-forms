/**
 * Wrapper for Antdesign component with redux
 * created by a8
 */

import React from "react";
import { Form } from "antd";
import moment from "moment";
import "./style.css";
import {
  Input,
  Select,
  Cascader,
  Radio as RadioHelper,
  Upload,
  Switch,
  DatePicker
} from "antd";
export const RenderField = Component => ({
  input,
  meta,
  children = null,
  formItemStyle = {},
  formItemLayout = {},
  hasFeedback,
  label,
  ...rest
}) => {
  const hasError = meta.touched && meta.invalid;
  const { value, onChange, ...filteredInput } = input;
  return (
    <Form.Item
      {...formItemLayout}
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
    >
      <Component
        value={input.value ? input.value : undefined}
        onChange={input.onChange}
        {...filteredInput}
        {...rest}
        children={children}
        defaultChecked={input.value}
      />
    </Form.Item>
  );
};

export const CascaderHelper = props => {
  const {
    input,
    cascaderProps,
    formItemStyle,
    meta,
    label = null,
    hasFeedback
  } = props;
  const hasError = meta.dirty && meta.invalid;
  return (
    <Form.Item
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
    >
      <Cascader
        value={input.value ? input.value : undefined}
        onChange={input.onChange}
        {...cascaderProps}
      />
    </Form.Item>
  );
};

export const DatePickerHelper = props => {
  const {
    input,
    datePickerProps,
    formItemStyle,
    meta,
    label = null,
    hasFeedback,
    ...extra
  } = props;
  const hasError = meta.dirty && meta.invalid;
  const { value, onChange, ...filteredInput } = input;
  return (
    <Form.Item
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
    >
      <DatePicker
        value={value ? moment(value) : null}
        onChange={onChange}
        {...filteredInput}
        {...extra}
      />
    </Form.Item>
  );
};

//connect antd element with redux-form
export const TextBox = RenderField(Input);

//conenct antd element with redux-form
export const TextAreaHelper = RenderField(Input.TextArea);

//select component
export const SelectHelper = RenderField(Select);

//Radio.Group Wrapper
export const RadioWrapper = RenderField(RadioHelper.Group);

//Export Radio Button 
export const RadioButton = RadioHelper.Button;

//Export Radio helper
export const Radio = RadioHelper;

//Upload wrapper with redux-form
export const UploadHelper = RenderField(Upload);

//Switch wrapper
export const SwitchWrapper = RenderField(Switch);

//Option Helper for Select Widget
export const Option = Select.Option;

//DatePicker Helper
// export const DatePickerHelper = RenderField(DatePicker);
